/**
 * Find top ten economical bowlers in the year 2015 
 * 
 * @param {Array} matches of IPL 
 * @param {Array} deliveries of IPL matches
 * @returns {object} Top ten economical bowlers in year 2015
 */

 function topTenEconomicalBowler(matches,deliveries){

  const ids = matches.filter(obj => obj.season === '2015').map(obj => parseInt(obj.id));
    
  const deliveries2015 = deliveries.filter(val => ids.includes(parseInt(val.match_id)));
    
  const totalRunsBalls = deliveries2015.reduce((total, delivery)=>{
    const bowler = delivery.bowler;
    
    if (total[bowler]) {
        total[bowler].bowls += 1;
        total[bowler].concededRun += parseInt(delivery.total_runs);
        total[bowler].economyRate =((total[bowler].concededRun / ((total[bowler].bowls) / 6)).toFixed(3));
    
    } else {
    
        total[bowler] = {};
        total[bowler].bowls = 1;
        total[bowler].concededRun = parseInt(delivery.total_runs);        
        total[bowler].economyRate =((total[bowler].concededRun / ((total[bowler].bowls) / 6)).toFixed(3));
    }
    
    return total;  
  }, {});
    
  const topTenEconomicalBowler = Object.entries(totalRunsBalls).sort((a, b) => a[1].economyRate - b[1].economyRate).slice(0, 10);
  let result = Object.fromEntries(topTenEconomicalBowler)
  
  return result
    
}


     


module.exports = topTenEconomicalBowler