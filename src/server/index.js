

const fs = require("fs")
const matches = "./src/data/matches.json"
const deliveries= "./src/data/deliveries.json"
const outputFilePath = "./src/public/output/"


let matchesPlayedPerYear = require("./ipl/matchesPlayedPerYear.js")
let matchesWonPerTeamEachYear = require("./ipl/matchesWonPerTeamEachYear.js")
let extraRunsConcededPerTeam = require("./ipl/extraRunsConcededPerTeam.js")
let topTenEconomicalBowler =  require("./ipl/topTenEconomicalBowler.js")
let eachTeamWonTossAndMatch = require("./ipl/eachTeamWonTossAndMatch.js")

// Read matches.json and delivery.json file

const matchesInJson = JSON.parse(fs.readFileSync(matches, 'utf8')); 
const deliveriesInJson = JSON.parse(fs.readFileSync(deliveries, 'utf8'));      
        
let result1 = matchesPlayedPerYear(matchesInJson)
let result2 = matchesWonPerTeamEachYear(matchesInJson)
let result3 = extraRunsConcededPerTeam(matchesInJson,deliveriesInJson)
let result4 = topTenEconomicalBowler(matchesInJson,deliveriesInJson)
let result5 = eachTeamWonTossAndMatch(matchesInJson)



const jsonData ={
        matchesPlayedPerYear: result1,
        matchesWonPerTeamEachYear: result2,
        extraRunsConcededPerTeam: result3,
        topTenEconomicalBowler: result4,
        eachTeamWonTossAndMatch: result5,     
       }

// write json content into the file

for(let index in jsonData){
      const jsonString = JSON.stringify(jsonData[index]);
      fs.writeFile(`${outputFilePath}${index}.json `, jsonString, "utf8", (err) => {
      if (err) {
      console.error(err);
    }
  });
}

        
