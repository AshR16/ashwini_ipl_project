/**
 * To calculate the number of times each team won the toss and the match.
 * 
 * @param {Array} matches of IPL  
 * @returns {object} Number of times won the toss and match by each team
 */

function eachTeamWonTossAndMatch(matches){
  let result = matches.reduce((tossAndMatch,match)=>{
    
      let winner = match.winner
      let tossWinner = match.toss_winner

      if(winner === tossWinner){
           if(tossAndMatch[winner]){
            tossAndMatch[winner]+=1
           }else{
            tossAndMatch[winner] = 1
           }
      }
      return tossAndMatch 
      
  },{})

  return result
}



module.exports = eachTeamWonTossAndMatch