/**
 * Calculate total number of matches played per year
 * 
 * @param {Array} matches of IPL 
 * @returns {object} total number of matches played for all the year in IPL
 */


function matchesPLayedPerYear(matches){
    
let result = matches.reduce((matchesPerYear,match)=>{
    let year = match.season
    if(matchesPerYear[year]){
        matchesPerYear[year] +=1
    }else{
        matchesPerYear[year] =1
    }
    return matchesPerYear

},{})
return result
   
}

module.exports = matchesPLayedPerYear