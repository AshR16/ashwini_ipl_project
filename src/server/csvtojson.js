const csv = require("csvtojson")
const fs = require("fs")

const csvMatches = "./src/data/matches.csv"
const csvDeliveries = "./src/data/deliveries.csv"

const jsonMatches = "./src/data/matches.json"
const jsonDeliveries = "./src/data/deliveries.json"


function csvToJson(jsonFilePath,csvFile){
    csv()
        .fromFile(csvFile)
        .then((json)=>{
            const jsonString = JSON.stringify(json);
            fs.writeFile( jsonFilePath,jsonString, "utf8", (err) => {
                if (err) {
                console.error(err);
              }
            });
        })
}

csvToJson(jsonMatches,csvMatches)
csvToJson(jsonDeliveries,csvDeliveries)