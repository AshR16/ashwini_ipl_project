/**
 * To calculate extra runs conceded per team per year in 2016 
 * 
 * @param {Array} matches of IPL 
 * @param {Array} deliveries of IPL matches
 * @returns {object} Total extra runs conceded per team in the year 2016
 */


function extraRunsConcededPerTeam(matches,deliveries){
  
    let ids = matches.filter(year=>year.season === "2016").map(obj=>parseInt(obj.id))
    let deliveryData = deliveries.filter(data => ids.includes(parseInt(data.match_id)))

    let result = deliveryData.reduce((extraRun,delivery)=>{
        let bowling_team = delivery["bowling_team"]
       if(extraRun[bowling_team]){
        extraRun[bowling_team]+= parseInt(delivery["extra_runs"])
       }else{
        extraRun[bowling_team]= parseInt(delivery["extra_runs"])
       }
       return extraRun
        
    },{})
  

  return result
}

module.exports=extraRunsConcededPerTeam