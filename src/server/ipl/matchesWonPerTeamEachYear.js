
/**
 * Calculate number of matches won per team per year in IPL.
 * 
 * @param {Array} matches of IPL 
 * @returns {object} Total number of matches won by each team in each season
 */


function matchesWonPerTeamEachYear(matches){
let result = matches.reduce((matchesWonByTeam,match)=>{
    let year = match.season
    let winner = match.winner

    if(matchesWonByTeam[year]){
        if(matchesWonByTeam[year][winner]){
            matchesWonByTeam[year][winner] += 1
        }else{
            matchesWonByTeam[year][winner] = 1
        }

    }else{
        matchesWonByTeam[year] = {}
    }
    return matchesWonByTeam

},{})

return result


}

module.exports= matchesWonPerTeamEachYear